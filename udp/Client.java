package udp;

import java.io.*;
import java.net.*;

/**
 * Client represents a UDP client that will send messages to another player.
 */
public class Client {
  String host;
  Integer hostPort;

  /**
   * Client constructor.
   * 
   * @param host     Opponent's host.
   * @param hostPort Opponent's port.
   */
  public Client(String host, Integer hostPort) {
    this.host = host;
    this.hostPort = hostPort;
  }

  /**
   * Sends a message to the opponent's host.
   * 
   * @param message The message to send.
   * @param timeout The time in seconds that the client will wait for a response.
   * 
   * @return String message received back by the client.
   */
  public String send(String message, Integer timeout) throws IOException {
    DatagramSocket socket = null;
    String returnMessage = null;

    try {
      // Creates and sends package
      socket = new DatagramSocket();
      socket.setSoTimeout(timeout * 1000);
      
      byte[] target = message.getBytes();
      DatagramPacket datagram = new DatagramPacket(
        target, 
        target.length, 
        InetAddress.getByName(this.host),
        this.hostPort
      );
      socket.send(datagram);

      // Waits for reply
      byte[] replyBuffer = new byte[1000];
      DatagramPacket replyDatagram = new DatagramPacket(replyBuffer, replyBuffer.length);
      socket.receive(replyDatagram);

      returnMessage = new String(replyDatagram.getData()).replaceAll("\\P{Print}","");
    } catch (SocketException e) {
      System.out.println("Client SocketException: " + e.getMessage());
    }
    
    if (socket != null)
      socket.close();

    return returnMessage;
  }
}