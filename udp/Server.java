package udp;

import java.io.*;
import java.net.*;

/**
 * Server represents a UDP server that will receive messages from another 
 * player.
 */
public class Server {
  Integer port;
  DatagramSocket socket;

  /**
   * Constructor for Server.
   * 
   * @param port Player's port.
   */
  public Server(Integer port) {
    this.port = port;
  }

  /**
   * Waits for a packet to be received from another host and returns the packet 
   * received.
   * 
   * @param timeout Time in seconds the method will wait for an incoming message.
   * @return DatagramPacket Packet received from another host.
   * @throws IOException Thrown if timeout exceeds.
   */
  public DatagramPacket receive(Integer timeout) throws IOException {
    DatagramPacket receivedDatagram = null;

    try {
      this.socket = new DatagramSocket(this.port);
      this.socket.setSoTimeout(timeout * 1000);

      byte[] buffer = new byte[1000];
      receivedDatagram = new DatagramPacket(buffer, buffer.length);
      this.socket.receive(receivedDatagram);

    } catch (SocketException e) {
      System.out.println("Could not receive message. Error: " + e.getMessage());
    }

    return receivedDatagram;
  }

  /**
   * Should be called immeaditely after the logic from the packet received from 
   * the receive method is processed. Effectively returns the request received in
   * the receive method.
   *  
   * @param message Message to be send back to the opponent.
   * @param host    Opponent's host that send the request.
   * @param port    Opponent's host's port.
   * @return void
   */
  public void reply(String message, InetAddress host, Integer port) {
    try {
      DatagramPacket confirmationDatagram = new DatagramPacket(
        message.getBytes(), 
        message.getBytes().length,
        host, 
        port
      );
      this.socket.send(confirmationDatagram);
    } catch (IOException e) {
      System.out.println("Could not respond to received message. Error: " + e.getMessage());
    } finally {
      this.socket.close();
    }
  }

  /**
   * Closes the socket opened in the receive method.
   * 
   * @return void
   */
  public void close() {
    if (this.socket != null) {
      this.socket.close();
    }
  }
}