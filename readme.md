# Tutorial
This is a two-player battleship game designed to run in your command prompt. You can play in the same computer on two different terminals, or in two different computers that are in the same network.

## Compiling
To compile the project, change directories into the project root and run the following:
```bash
  javac -d ./class/ Main.java
```

## Running
After compiling, you can start the game by running:
```bash
  java -cp ./class/ Main
```

## Setup
The game will ask you the following initial data:

<br />


You must indicate which player you will be. Only the numbers 1 and 2 are accepted.
```
Will you be player 1 or 2? (Type either 1 or 2):
```

<br />


You must indicate if you and your opponent will be playing from the same computer. Only the letters y and n are accepted, for yes and no respectively.
```
Will you be playing in the same computer? (Type either y or n):
```

If you are playing from two different computers, you must tell the game your opponent's local IP address. Only valid IPv4 addresses are accepted.
```
Will you be playing in the same computer? (Type either y or n): n
What is your opponent's IP address? (Type a valid IPv4 host address):
```

## Gameplay
Your objective is to sink all of your opponent's battleships. The game will generate both of your fields automatically, placing the following ships:

- 1 ship of size 3
- 2 ships of size 2
- 3 ships of size 1

Your grid might initially look like this:
```
 YOUR FIELD:
   | A | B | C | D | E | F | G | H | I | J | 
1  | 2 | 2 |   |   |   |   |   |   |   |   |
2  |   |   |   |   |   |   |   |   |   |   |
3  |   |   |   |   |   |   |   |   |   |   |
4  |   |   |   |   |   |   |   |   |   |   |
5  |   |   |   |   |   |   |   |   |   |   |
6  |   |   | 3 |   |   |   |   |   |   | 1 |
7  |   |   | 3 |   |   |   |   |   |   | 1 |
8  |   |   | 3 |   |   |   |   |   |   |   |
9  |   |   |   |   |   |   | 2 |   |   |   |
10 |   | 1 |   |   |   |   | 2 |   |   |   |
TARGETS HIT: 0/10
```

And your opponent's grid will be initially blank for you:
```
OPPONENT'S FIELD:
   | A | B | C | D | E | F | G | H | I | J | 
1  |   |   |   |   |   |   |   |   |   |   |
2  |   |   |   |   |   |   |   |   |   |   |
3  |   |   |   |   |   |   |   |   |   |   |
4  |   |   |   |   |   |   |   |   |   |   |
5  |   |   |   |   |   |   |   |   |   |   |
6  |   |   |   |   |   |   |   |   |   |   |
7  |   |   |   |   |   |   |   |   |   |   |
8  |   |   |   |   |   |   |   |   |   |   |
9  |   |   |   |   |   |   |   |   |   |   |
10 |   |   |   |   |   |   |   |   |   |   |
TARGETS HIT: 0/10
```

Player 1 goes first. The game will ask them a target in the opponent's field to shoot:
```
It's your turn! You have 60 seconds to shoot!
Target (Must be a letter (a-h) and a number (1-10) typed together):
```
Only valid targets are accepted. The letters may be either upper or lower case.

If you take more than the described amount of time, your opponent will lose connection with you, and when you do shoot, you will eventually lose connection as well.

You and your opponent will each take turns shooting each other's fields. If one of you hit the other's ship, you will go again. You will keep shooting until you miss.

Hits will be displayed with an 'X', and misses will be displayed with an 'o'.

The following grid has 3 hits and 4 misses:
```
YOUR FIELD:
   | A | B | C | D | E | F | G | H | I | J | 
1  |   |   |   |   |   | o |   |   |   |   |
2  | 1 |   |   |   |   |   |   |   | 3 |   |
3  |   |   |   |   |   |   |   |   | 3 |   |
4  |   |   |   |   |   |   |   | o | 3 |   |
5  | X | X |   | o |   |   | 2 | 2 |   |   |
6  |   |   |   |   |   |   |   |   |   |   |
7  |   |   |   |   |   |   |   |   |   | o |
8  |   | 1 |   |   |   |   |   |   |   |   |
9  |   |   |   |   |   |   |   | X |   |   |
10 |   |   |   |   |   |   |   |   |   |   |
TARGETS HIT: 3/10
```

The game will end when one of the players hit 10 of their opponent's targets.

Enjoy!