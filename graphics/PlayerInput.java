package graphics;

import java.util.Scanner;
import java.net.InetAddress;
import java.util.Collections;
import java.util.Enumeration;
import java.net.SocketException;
import java.net.NetworkInterface;


import validation.*;

/**
 * Retrieves input from the player.
 */
public class PlayerInput {
  /**
   * Returns if player will be player 1 or 2.
   *
   * @return Boolean True if player 1.
   */
  public static Boolean isP1() {
    String p1OrP2 = null;
      do {
      System.out.print("Will you be player 1 or 2? (Type either 1 or 2): ");
      p1OrP2 = (new Scanner(System.in)).next();
    } while (!PlayerInputValidation.playerOneOrTwo(p1OrP2));

    return p1OrP2.equals("1");
  }

  /**
   * Returns if players will be playing from the same computer.
   *
   * @return Boolean True if playing from same computer.
   */
  public static Boolean isSameComputer() {
    String sameComputerStr = null;
      do {
        System.out.print("Will you be playing in the same computer? (Type either y or n): ");
        sameComputerStr = (new Scanner(System.in)).next();
      } while (!PlayerInputValidation.yesOrNo(sameComputerStr));

    return  sameComputerStr.equals("y");
  }

  /**
   * Retrieves the opponent's host. If players are playing from the 
   * same computer, returns the local 192.168 series IP address.
   *
   * @param sameComputer If players are playing from the same computer.
   *
   * @return String Opponent's host.
   */
  public static String getHost(Boolean sameComputer) {
    String host = null;

    try {
      if (sameComputer) {
        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
        for (NetworkInterface netint : Collections.list(nets)) {
          Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
          for (InetAddress inetAddress : Collections.list(inetAddresses)) {
            if (inetAddress.toString().contains("192.168")) {
              host = inetAddress.toString().substring(1);
            }
          }
        }
      } else {
        do {
          System.out.print("What is your opponent's IP address? (Type a valid IPv4 host address): ");
          host = (new Scanner(System.in)).next();
        } while (!PlayerInputValidation.validHost(host));
      }
    } catch (SocketException e) {
      System.out.println("Somehting went wrong while getting your IP address. Error: " + e);
    }

    return host;
  }
}