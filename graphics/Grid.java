package graphics;

/**
 * Grid is able to generate a random grid with ships places inside of it and
 * track its changes (when a ship is hit).
 * 
 * Internal Grid codes: 1 - 3: Ship position. The number displayed indicates its
 * size. -1: Miss. -2: Hit. null: Nothing.
 */
public class Grid {
  Integer height;
  Integer width;

  Integer[] ships;

  Integer[][] grid;

  Integer hitCount;
  Integer maxHits;

  /**
   * Grid constructor.
   * 
   * @param height Grid's height.
   * @param width  Grid's width.
   */
  public Grid(Integer height, Integer width) {
    this.height = height;
    this.width = width;

    this.ships = new Integer[] { 1, 1, 1, 2, 2, 3 };
    this.grid = new Integer[this.height][this.width];

    this.hitCount = 0;
    this.maxHits = 10;
  }

  /**
   * Populates the grid with ships described in the constructor.
   */
  public void populate() {
    this.grid = new Integer[this.height][this.width];
    for (Integer ship : this.ships) {
      Boolean set = false;

      Integer randomHeight = null;
      Integer randomWidth = null;
      Boolean down = null;

      while (!set) {
        randomHeight = (int) (Math.random() * this.height);
        randomWidth = (int) (Math.random() * this.height);
        down = ((int) (Math.random() * 2)) == 0;

        if (this.grid[randomWidth][randomHeight] == null) {
          set = true;
          for (int b = 1; b < ship; b++) {
            try {
              Integer newHeight = (down ? randomHeight + b : randomHeight);
              Integer newWidth = (down ? randomWidth : randomWidth + b);

              if (this.grid[newWidth][newHeight] != null || newHeight > this.height || newWidth > this.width) {
                set = false;
                break;
              }
            } catch (ArrayIndexOutOfBoundsException e) {
              set = false;
              break;
            }
          }
        }

        if (set) {
          for (int b = 0; b < ship; b++) {
            Integer newHeight = (down ? randomHeight + b : randomHeight);
            Integer newWidth = (down ? randomWidth : randomWidth + b);

            this.grid[newWidth][newHeight] = ship;
          }

        }
      }
      // System.out.printf("%s; %s%s %s \n", boat, ((char) (randomHeight + 65)),
      // (randomWidth + 1), (down ? "Down" : "Across"));
    }
  }

  /**
   * Prints the grid on the console.
   */
  public void print() {
    // Letter header
    char a = 'A';
    System.out.print("   | ");
    for (int letter = a; letter < (a + this.height); letter++) {
      System.out.print(((char) letter) + " | ");
    }
    System.out.println();

    // Number Column and Grid
    for (int i1 = 1; i1 <= this.width; i1++) {
      System.out.print(i1 + (Integer.toString(i1).length() == 1 ? "  |" : " |"));
      for (int i2 = 0; i2 < this.height; i2++) {
        if (this.grid[i1 - 1][i2] != null && this.grid[i1 - 1][i2] == -1) {
          System.out.print(" o |");
        } else if (this.grid[i1 - 1][i2] != null && this.grid[i1 - 1][i2] == -2) {
          System.out.print(" X |");
        } else if (this.grid[i1 - 1][i2] != null) {
          System.out.printf(" %s |", this.grid[i1 - 1][i2]);
        } else {
          System.out.print("   |");
        }
      }
      System.out.println();
    }

    System.out.printf("TARGETS HIT: %s/%s\n", this.hitCount, this.maxHits);
  }

  /**
   * Verifies a strike at a position in the grid and returns the code of the
   * strike. The codes are described in the Grid class JavaDocs.
   * 
   * @param height X position of strike.
   * @param width  Y posistion of strike.
   * 
   * @return Integer Code of postion hit.
   */
  public Integer receiveStrike(char height, Integer width) {
    if (this.grid[(width - 1)][(((int) height) - 65)] == null || this.grid[(width - 1)][(((int) height) - 65)] == -1) {
      this.grid[(width - 1)][(((int) height) - 65)] = -1;
      return -1;
    } else {
      Integer hit = this.grid[(width - 1)][(((int) height) - 65)];

      if (hit != -2) {
        this.hitCount++;
      }

      this.grid[(width - 1)][(((int) height) - 65)] = -2;

      return hit;
    }
  }

  /**
   * Updates the grid mannually with a value.
   * 
   * @param height X position of strike.
   * @param width  Y posistion of strike.
   * @param hit    Position code.
   */
  public void setHit(char height, Integer width, Integer hit) {
    if (hit != -1) {
      this.hitCount++;
    }

    this.grid[(width - 1)][(((int) height) - 65)] = hit;
  }

  /**
   * Verifies if all ships inside the grid are sunk.
   * 
   * @return Boolean True if all ships were hit.
   */
  public Boolean allShipsAreSunk() {
    return this.hitCount == this.maxHits;
  }
}