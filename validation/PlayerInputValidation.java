package validation;

/**
 * Class used to validate user input.
 */
public class PlayerInputValidation {

  /**
   * Validates the target input.
   * Must be composed of a letter between A and H and a number between 1 and 10.
   * 
   * @param target Player input.
   *
   * @return Boolean True if valid.
   */
  public static Boolean target(String target) {
    try {
      return (
        (target.length() == 2 || target.length() == 3) &&
        (
          (target.charAt(0) >= 65 && target.charAt(0) <= 74) ||
          (target.charAt(0) >= 97 && target.charAt(0) <= 106)
        ) &&
        (
          Integer.parseInt(target.substring(1)) >= 1 &&
          Integer.parseInt(target.substring(1)) <= 10
        )
      );
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Validates the player's input for which player they will be (1/2).
   * Must be either 1 or 2.
   * 
   * @param input Player input.
   *
   * @return Boolean True if valid.
   */
  public static Boolean playerOneOrTwo(String input) {
    return input.equals("1") || input.equals("2");
  }

  /**
   * Validates the player's input for a yes or no question.
   * Must be either y or n.
   * 
   * @param input Player input.
   *
   * @return Boolean True if valid.
   */
  public static Boolean yesOrNo(String input) {
    return input.equals("y") || input.equals("n");
  }

  /**
   * Validates the player's input for an IP address.
   * Must be either y or n.
   * 
   * @param input Player input.
   *
   * @return Boolean True if valid.
   */
  public static Boolean validHost(String ip) {
    try {
      if ( ip == null || ip.isEmpty() ) {
        return false;
      }

      String[] parts = ip.split( "\\." );
      if ( parts.length != 4 ) {
        return false;
      }

      for ( String s : parts ) {
        int i = Integer.parseInt( s );
        if ( (i < 0) || (i > 255) ) {
          return false;
        }
      }
      if ( ip.endsWith(".") ) {
        return false;
      }

      return true;
    } catch (NumberFormatException nfe) {
      return false;
    }
  }
}