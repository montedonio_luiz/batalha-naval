import java.io.IOException;

import exception.*;
import validation.*;
import graphics.PlayerInput;

/**
 * This battleship program is a terminal-based game between two players. It is
 * playable from either two different terminals in the same computer, or from
 * two different computers in the same LAN. You will be prompted all host
 * settings upon initialization.
 * 
 * @author Luiz Oliveira Montedônio
 * @version 1.0
 */
public class Main {
  public static void main(String[] args) {
    Integer p1Port = 1234;
    Integer p2Port = 12345;

    try {
      System.out.println("Welcome to battleship!\n");

      Boolean isP1 = PlayerInput.isP1();
      String host = PlayerInput.getHost(PlayerInput.isSameComputer());

      // Create Player
      Player player = new Player(isP1 ? p1Port : p2Port, host, isP1 ? p2Port : p1Port, isP1);

      // Look for Player
      try {
        if (player.isP1) {
          player.lookForP2();
        } else {
          player.lookForP1();
        }
      } catch (PlayerNotFoundException e) {
        System.out.println(e.getMessage());
      }

      // Start Game
      player.setupGrids();
      System.out.printf("You are: %s\n\n ", isP1 ? "Player 1" : "Player 2");

      player.showGrids();

      if (isP1) {
        player.shoot();
      }

      while (true) {
        player.standBy();
        player.showGrids();
        player.shoot();
      }

    } catch (IOException e) {
      System.out.println("You've lost connection to your opponent. Error: " + e.getMessage());
    } catch (PlayerVictoryException e) {
      System.out.println(e.getMessage());
    } catch (PlayerDefeatException e) {
      System.out.println(e.getMessage());
    }
  }
}