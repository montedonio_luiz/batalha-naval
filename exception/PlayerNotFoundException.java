package exception;

/**
 * Exception meant for when a player is unseccessful in connection with their
 * opponent.
 */
public class PlayerNotFoundException extends Exception {

  private static final long serialVersionUID = 1L;

  /**
   * Constructor for PlayerNotFoundException
   * 
   * @param player Player that the game was searching.
   * @param host   Host where the player shuold have been.
   * @param port   Port where the player shuold have been.
   */
  public PlayerNotFoundException(Integer player, String host, Integer port) {
    super(
      String.format(
        "\nConnection with your opponent was unsuccessful. Make sure there is a player %s waiting to connect with you on %s:%s and try again. \n",
        player, 
        host, 
        port
      )
    );
  }
}