package exception;

public class PlayerVictoryException extends Exception {

  private static final long serialVersionUID = 1L;

  public PlayerVictoryException() {
    super("Victory!\nThanks for playing battleship!");
  }
  
}