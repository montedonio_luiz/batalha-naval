package exception;

public class PlayerDefeatException extends Exception {

  private static final long serialVersionUID = 1L;

  public PlayerDefeatException() {
    super("Game over!\nBetter luck next time.\nThanks for playing battleship!");
  }
  
}