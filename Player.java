import java.io.IOException;
import java.net.*;
import java.util.Scanner;

import exception.*;
import validation.*;
import graphics.Grid;
import udp.*;

/**
 * Player represents each player in the game. Each section of the game (grid
 * setup, connection with the other player, and user input) is called and used
 * here.
 */
public class Player {
  Integer playerPort;

  String opponentHost;
  Integer opponentPort;

  Boolean isP1;

  Client client;
  Server server;

  Integer turnTime;
  Integer syncTime;

  Grid playerGrid;
  Grid enemyGrid;

  /**
   * Player constructor.
   * 
   * @param playerPort   Port for the player.
   * @param opponentHost Host for the player's opponent. If playing on the same
   *                     computer, both players will have the same host.
   * @param opponentPort Port for the player's opponent. If playing on the same
   *                     computer, the players' ports must be different.
   * @param isP1         Indicates if this player will be player one (if they will
   *                     shoot first).
   */
  public Player(Integer playerPort, String opponentHost, Integer opponentPort, Boolean isP1) {
    this.playerPort = playerPort;

    this.opponentHost = opponentHost;
    this.opponentPort = opponentPort;

    this.isP1 = isP1;

    this.client = new Client(opponentHost, opponentPort);
    this.server = new Server(playerPort);

    this.turnTime = 60;
    this.syncTime = 10;
  }

  /**
   * Searches for a player 1. At the beggining of the game, this method should be
   * called to search for the player's opponent with the received settings (host
   * and port). Both players should start the game within the same 5 seconds and
   * get to this point of execution at roughly the same time. If no other player
   * is found within 60 seconds, the game will not start.
   * 
   * @return void
   */
  public void lookForP1() throws PlayerNotFoundException {
    System.out.println("Looking for a player 1\n");

    Integer searchProgressCap = (this.syncTime / 2);
    Integer[] searchProgress = new Integer[] { 0, searchProgressCap };

    DatagramPacket receivedPacket = null;
    while (searchProgress[0] <= this.syncTime) {
      try {
        receivedPacket = this.server.receive((this.syncTime / searchProgress[1]));
        break;
      } catch (IOException e) {
        this.server.close();
        searchProgress[0] += (this.syncTime / searchProgress[1]);
        System.out.print(".");
      }
    }

    if (receivedPacket != null && new String(receivedPacket.getData()).replaceAll("\\P{Print}", "").equals("ACK")) {
      System.out.println("Connection succesfull");
      this.server.reply("NAK", receivedPacket.getAddress(), receivedPacket.getPort());
    } else {
      throw new PlayerNotFoundException(1, this.opponentHost, this.opponentPort);
    }
  }

  /**
   * Searches for a player 2. At the beggining of the game, this method should be
   * called to search for the player's opponent with the received settings (host
   * and port). Both players should start the game within the same 5 seconds and
   * get to this point of execution at roughly the same time. If no other player
   * is found within 60 seconds, the game will not start.
   * 
   * @return void
   */
  public void lookForP2() throws PlayerNotFoundException {
    System.out.println("Looking for a player 2\n");

    Integer searchProgressCap = (this.syncTime / 2);
    Integer[] searchProgress = new Integer[] { 0, searchProgressCap };

    String response = null;
    while (searchProgress[0] <= this.syncTime) {
      try {
        response = this.client.send("ACK", (this.syncTime / searchProgress[1]));
        break;
      } catch (IOException e) {
        searchProgress[0] += (this.syncTime / searchProgress[1]);
        System.out.print(".");
      }
    }

    if (response != null && response.equals("NAK")) {
      System.out.println("Connection succesfull");
    } else {
      throw new PlayerNotFoundException(2, this.opponentHost, this.opponentPort);
    }
  }

  public void setupGrids() {
    this.playerGrid = new Grid(10, 10);
    this.playerGrid.populate();

    this.enemyGrid = new Grid(10, 10);
  }

  public void showGrids() {
    System.out.println("YOUR FIELD:");
    this.playerGrid.print();
    System.out.println("\nOPPONENT'S FIELD:");
    this.enemyGrid.print();
    System.out.println();
  }

  /**
   * Represents a player's turn in the game. This method will ask the player for a
   * target in the opponnent's grid and will send it via UDP socket communication
   * to the opponent's host, and wait for a reply to process if it was a hit or a
   * miss.
   * 
   * @return void
   */
  public void shoot() throws IOException, PlayerVictoryException {
    System.out.printf("It's your turn! You have %s seconds to shoot!\n", this.turnTime);

    String target = "";

    do {
      System.out.print("Target (Must be a letter (a-h) and a number (1-10) typed together): ");
      target = (new Scanner(System.in)).next();
    } while (!PlayerInputValidation.target(target));

    target = target.toUpperCase();

    String report = this.client.send(target, this.turnTime);

    System.out.println("\n\n\n\n\n\n");
    if (report.equals("-2")) {
      System.out.println("We've already hit this target, and have lost the turn!\n");

      System.out.println("OPPONENT'S FIELD:");
      this.enemyGrid.print();
    } else {
      this.enemyGrid.setHit(target.charAt(0), Integer.parseInt(target.substring(1)), Integer.parseInt(report));

      if (report.equals("-1")) {
        System.out.println("Darn! We didn't hit anything.\n");

        System.out.println("OPPONENT'S FIELD:");
        this.enemyGrid.print();
      } else {
        System.out.printf("We hit a ship of size %s!\n\n", report);

        System.out.println("OPPONENT'S FIELD:");
        this.enemyGrid.print();

        if (!this.enemyGrid.allShipsAreSunk()) {
          this.shoot();
        } else {
          throw new PlayerVictoryException();
        }
      }
    }
  }

  /**
   * After each shot, this method should be called. It opens a UDP server and
   * waits until the opponent sends his shot, process it as a hit/miss upon
   * receival, and send back this indormation to the opponent.
   * 
   * @return void
   */
  public void standBy() throws IOException, PlayerDefeatException {
    System.out.println("Scouting...\n\n\n\n\n\n\n");

    DatagramPacket target = null;

    target = this.server.receive(this.turnTime);

    String guess = new String(target.getData()).replaceAll("\\P{Print}", "");

    Integer hit = this.playerGrid.receiveStrike(guess.charAt(0), Integer.parseInt(guess.substring(1)));

    switch (hit) {
      case -1: {
        System.out.printf("Incoming! The enemy striked position %s and missed.\n\n", new String(target.getData()));

        this.server.reply(hit.toString(), target.getAddress(), target.getPort());
        break;
      }
      case -2: {
        System.out.printf("The enemy has attacked our ship's ruins at %s. What a dummy!\n\n",
            new String(target.getData()));

        this.server.reply(hit.toString(), target.getAddress(), target.getPort());
        break;
      }
      default: {
        System.out.printf("Incoming! The enemy striked position %s and one of our ships was hit.\n\n",
            new String(target.getData()));

        this.server.reply(hit.toString(), target.getAddress(), target.getPort());

        System.out.println("YOUR FIELD:");
        this.playerGrid.print();

        if (!this.playerGrid.allShipsAreSunk()) {
          this.standBy();
        } else {
          throw new PlayerDefeatException();
        }

      }
    }
  }

  public void closeServer() {
    this.server.close();
  }

}